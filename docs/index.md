# Willkommen auf unserer Spieleseite!
Anfang 2024 haben wir begonnen, gemeinsam mit Julian Scratch-Spiele zu entwickeln. Es macht uns beiden sehr viel Spaß!
Diese Seite dient als Sammelstelle für Spiele, die Julian und ich seitdem zusammen konzipiert und entwickelt haben.

Viel Spaß beim Stöbern und Spielen!

*Alle Spielideen unterliegen dem Copyright © 2024, Julian Linke & Amir Teymuri.*
