# Die Schulwand
Auf der Vorderwand der Weinbrennerschule in Karlsruhe befindet sich eine schöne Kette von 26 zusammengesetzten Nomen. Dabei beginnt jedes nächste zusammengesetzte Nomen mit dem zweiten Teil des vorherigen. Die Kette ist zirkulär aufgebaut, sodass das letzte Nomen (Gusseisen) auf der Wand nach diesem Prinzip wieder zum ersten (Eisenbahn) führt. Am Samstag, den 16.03.2024, standen wir vor dieser Wand und entwickelten die folgende Spielidee: Du darfst Dir die Wand für eine beliebige Zeit ansehen und Dir die Nomen merken. Danach wird Dir ein zufälliges Nomen aus der Wand vorgegeben. Die Aufgabe ist es, die X Nomen davor und X Nomen, die nach dem vorgegebenen Nomen auf der Wand erscheinen, zu schreiben (ohne auf die Wand zu schauen!), wobei X eine beliebige Zahl ist, die Du selbst bestimmst. Drücke die grüne Flagge, um mit dem Spielen zu beginnen.

<iframe src="https://scratch.mit.edu/projects/984274189/embed" allowtransparency="true" width="485" height="402" frameborder="0" scrolling="no" allowfullscreen></iframe>

17.03.2024
